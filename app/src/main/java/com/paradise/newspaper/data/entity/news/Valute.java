package com.paradise.newspaper.data.entity.news;

public class Valute implements News {

    private String usd, eur, date;

    public Valute(String usd, String eur, String date) {
        this.usd = usd;
        this.eur = eur;
        this.date = date;
    }

    @Override
    public int getType() {
        return TYPE_VALUTE_EXCHANGE;
    }

    @Override
    public long getCreationDate() {
        return Long.parseLong(date);
    }

    public String getUsd() {
        return usd;
    }

    public String getEur() {
        return eur;
    }

    public String getDate() {
        return date;
    }
}
