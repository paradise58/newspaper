package com.paradise.newspaper.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.paradise.newspaper.data.entity.news.Basic;
import io.reactivex.Flowable;

import java.util.List;

@Dao
public interface NewsBasicDao {

    @Query("SELECT * FROM Basic")
    Flowable<List<Basic>> getAll();

    @Insert
    void insert(Basic basic);

    @Insert
    void insert(List<Basic> list);
}
