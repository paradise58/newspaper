package com.paradise.newspaper.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.paradise.newspaper.data.entity.news.Youtube;
import io.reactivex.Flowable;

import java.util.List;

@Dao
public interface NewsYoutubeDao {

    @Query("SELECT * FROM Youtube")
    Flowable<List<Youtube>> getAll();

    @Insert
    void insert(Youtube youtube);

    @Insert
    void insert(List<Youtube> list);
}
