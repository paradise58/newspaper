package com.paradise.newspaper.data.entity.news;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity
public class Youtube implements News {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private long creationDate;
    private String videoId;

    public Youtube(String videoId, long creationDate) {
        this.videoId = videoId;
        this.creationDate = creationDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public int getType() {
        return News.TYPE_YOUTUBE;
    }
}
