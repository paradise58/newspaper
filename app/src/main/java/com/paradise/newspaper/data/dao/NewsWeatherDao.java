package com.paradise.newspaper.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.paradise.newspaper.data.entity.weather.Weather;
import io.reactivex.Flowable;

import java.util.List;

@Dao
public interface NewsWeatherDao {

    @Query("SELECT * FROM Weather")
    List<Weather> getList();

    @Query("SELECT * FROM Weather")
    Flowable<List<Weather>> getAll();

    @Insert
    void insert(Weather weather);

    @Insert
    void insert(List<Weather> weathers);

    @Delete
    void deleteAll(List<Weather> weathers);

}


