package com.paradise.newspaper.data.network;

import com.paradise.newspaper.data.entity.weather.fiveDays.FiveDaysWeatherResult;
import com.paradise.newspaper.data.entity.weather.oneDay.OneDayWeatherResult;
import io.reactivex.Observable;
import retrofit2.http.GET;

public interface IWeatherClient {

    String LAT = "53.170545";
    String LNG = "44.006512";
    String APPID = "1afb8105b5238f1fd2ad2e21caa7417d";

    @GET("weather?" + "lat=" + LAT + "&lon=" + LNG + "&units=metric" + "&lang=ru" + "&appid=" + APPID)
    Observable<OneDayWeatherResult> getWeatherByLatLng();

    @GET("forecast?" + "lat=" + LAT + "&lon=" + LNG + "&units=metric" + "&lang=ru" + "&appid=" + APPID)
    Observable<FiveDaysWeatherResult> getForecastByLatLng();

}