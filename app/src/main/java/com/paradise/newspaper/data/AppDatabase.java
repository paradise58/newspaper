package com.paradise.newspaper.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import com.paradise.newspaper.data.dao.FirstLaunchDao;
import com.paradise.newspaper.data.dao.NewsBasicDao;
import com.paradise.newspaper.data.dao.NewsWeatherDao;
import com.paradise.newspaper.data.dao.NewsYoutubeDao;
import com.paradise.newspaper.data.dao.PartDao;
import com.paradise.newspaper.data.entity.FirstLaunch;
import com.paradise.newspaper.data.entity.news.Basic;
import com.paradise.newspaper.data.entity.news.Part;
import com.paradise.newspaper.data.entity.news.Youtube;
import com.paradise.newspaper.data.entity.weather.Weather;

@Database(entities = {FirstLaunch.class,
        Basic.class,
        Youtube.class,
        Weather.class,
        Part.class},
        version = 1,
        exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {

    public abstract FirstLaunchDao firstLaunchDao();

    public abstract NewsBasicDao newsBasicDao();

    public abstract NewsYoutubeDao newsYoutubeDao();

    public abstract NewsWeatherDao newsWeatherDao();

    public abstract PartDao partDao();
}
