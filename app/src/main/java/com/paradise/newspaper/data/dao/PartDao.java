package com.paradise.newspaper.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.paradise.newspaper.data.entity.news.Part;
import io.reactivex.Flowable;

import java.util.List;

@Dao
public interface PartDao {

    @Query("SELECT * FROM Part WHERE uuid = :uuid")
    Flowable<List<Part>> getByUUID(String uuid);

    @Insert
    void insert(List<Part> list);
}
