package com.paradise.newspaper.presentation.view.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.paradise.newspaper.R;
import com.paradise.newspaper.common.App;
import com.paradise.newspaper.presentation.adapter.PhotoDetailAdapter;
import com.paradise.newspaper.presentation.view.DetailActivity;
import com.paradise.newspaper.presentation.viewmodel.DetailViewModel;
import com.paradise.newspaper.presentation.viewmodel.GlobalViewModelFactory;
import com.paradise.newspaper.presentation.viewmodel.PhotoDetailState;

import javax.inject.Inject;

public class FragmentDetailPhotoContainer extends Fragment {

    @Inject
    GlobalViewModelFactory factory;
    DetailViewModel viewModel;
    private PhotoDetailState state;

    public static FragmentDetailPhotoContainer newInstance() {
        return new FragmentDetailPhotoContainer();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent(getContext()).inject(this);
        viewModel = ViewModelProviders.of(((DetailActivity) getActivity()), factory).get(DetailViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_detail_photo, container, false);
        ViewPager viewPager = inflate.findViewById(R.id.view_pager);
        viewModel.getPhotosDetails().observe(this, state -> {
            if (this.state != null) return;
            this.state = state;
            PhotoDetailAdapter adapter = new PhotoDetailAdapter(getChildFragmentManager(), this.state);
            viewPager.setAdapter(adapter);
            viewPager.setOffscreenPageLimit(5);
            viewPager.setCurrentItem(this.state.getPosition());
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                viewModel.goToDetailPhoto(position, state.getImageUrlList());
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        return inflate;
    }
}
