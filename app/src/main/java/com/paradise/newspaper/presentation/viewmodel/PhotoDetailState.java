package com.paradise.newspaper.presentation.viewmodel;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class PhotoDetailState implements Parcelable {

    private int position;
    private List<String> imageUrlList;

    public PhotoDetailState(int position, List<String> imageUrlList) {
        this.position = position;
        this.imageUrlList = imageUrlList;
    }

    public int getPosition() {
        return position;
    }

    public List<String> getImageUrlList() {
        return imageUrlList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.position);
        dest.writeStringList(this.imageUrlList);
    }

    protected PhotoDetailState(Parcel in) {
        this.position = in.readInt();
        this.imageUrlList = in.createStringArrayList();
    }

    public static final Creator<PhotoDetailState> CREATOR = new Creator<PhotoDetailState>() {
        @Override
        public PhotoDetailState createFromParcel(Parcel source) {
            return new PhotoDetailState(source);
        }

        @Override
        public PhotoDetailState[] newArray(int size) {
            return new PhotoDetailState[size];
        }
    };
}
