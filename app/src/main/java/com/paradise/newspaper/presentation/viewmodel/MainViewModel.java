package com.paradise.newspaper.presentation.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import com.paradise.newspaper.common.ActionLiveEvent;
import com.paradise.newspaper.common.App;

import javax.inject.Inject;

public class MainViewModel extends AbsViewModel {

    private final App context;
    private final MutableLiveData<Boolean> bottomSheetState = new MutableLiveData<>();
    private final ActionLiveEvent<Long> showDetail = new ActionLiveEvent<>();


    @Inject
    public MainViewModel(App context) {
        this.context = context;
    }


    public MutableLiveData<Boolean> getBottomSheetState() {
        return bottomSheetState;
    }

    public void setBottomSheetState(boolean visible) {
        bottomSheetState.setValue(visible);
    }

    public void showDetail(long id) {
        showDetail.setValue(id);
    }

    public ActionLiveEvent<Long> getShowDetail() {
        return showDetail;
    }
}
