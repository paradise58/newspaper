package com.paradise.newspaper.presentation.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.paradise.newspaper.R;
import com.paradise.newspaper.common.App;
import com.paradise.newspaper.data.entity.news.News;
import com.paradise.newspaper.presentation.adapter.NewsListAdapter;
import com.paradise.newspaper.presentation.viewmodel.GlobalViewModelFactory;
import com.paradise.newspaper.presentation.viewmodel.MainViewModel;
import com.paradise.newspaper.presentation.viewmodel.NewsListViewModel;

import javax.inject.Inject;
import java.util.List;

public class NewsListFragment extends Fragment {

    @Inject
    GlobalViewModelFactory factory;
    NewsListViewModel viewModel;
    MainViewModel mainViewModel;

    private RecyclerView newsRecycler;
    private NewsListAdapter newsAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent(getContext()).inject(this);
        viewModel = ViewModelProviders.of(this, factory).get(NewsListViewModel.class);
        mainViewModel = ViewModelProviders.of(((MainActivity) getActivity()), factory).get(MainViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news_list, container, false);
        initializeRecyclerView(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.getNewsLiveData().observe(this, this::renderData);
    }

    private void renderData(List<News> news) {
        if (news.isEmpty()) return;
        newsAdapter.setItems(news);
    }

    private void initializeRecyclerView(View view) {
        newsRecycler = view.findViewById(R.id.newsList);
        newsRecycler.setNestedScrollingEnabled(false);
        newsRecycler.setHasFixedSize(false);
        newsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        newsRecycler.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        newsAdapter = new NewsListAdapter(getActivity().getSupportFragmentManager(), newsRecycler, this.getLifecycle(), mainViewModel);
        newsRecycler.setAdapter(newsAdapter);
    }
}
