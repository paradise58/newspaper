package com.paradise.newspaper.presentation.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import com.paradise.newspaper.common.Constants;
import com.paradise.newspaper.common.SchedulerProvider;
import com.paradise.newspaper.data.entity.news.Part;
import com.paradise.newspaper.data.network.StubFB;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

public class DetailViewModel extends AbsViewModel {


    private final StubFB stubFB;
    private final SchedulerProvider schedulerProvider;

    private MutableLiveData<List<Part>> newsParts = new MutableLiveData<>();
    private MutableLiveData<PhotoDetailState> photosDetails = new MutableLiveData<>();
    private MutableLiveData<Integer> showingFragment = new MutableLiveData<>();
    private int currentShowingFragment;

    public void setCurrentShowingFragment(int newShowingFragment) {
        this.currentShowingFragment = newShowingFragment;
        showingFragment.setValue(currentShowingFragment);
    }

    @Inject
    public DetailViewModel(StubFB stubFB,
                           SchedulerProvider schedulerProvider) {
        this.stubFB = stubFB;
        this.schedulerProvider = schedulerProvider;
        setCurrentShowingFragment(Constants.DETAIL_NEWS);
    }

    public void setUUID(String uuid) {
        compositeDisposable.add(stubFB.getPartDao().getByUUID(uuid)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.mainThread())
                .subscribe(list -> {
                    Collections.sort(list, (o1, o2) -> Integer.compare(o1.getOrder(), o2.getOrder()));
                    newsParts.setValue(list);
                }));
    }

    public MutableLiveData<List<Part>> getNewsParts() {
        return newsParts;
    }


    public void goToDetailPhoto(int position, List<String> imageUrlList) {
        if (currentShowingFragment != Constants.DETAIL_PHOTO) {
            setCurrentShowingFragment(Constants.DETAIL_PHOTO);
        }
        photosDetails.setValue(new PhotoDetailState(position, imageUrlList));
    }

    public MutableLiveData<PhotoDetailState> getPhotosDetails() {
        return photosDetails;
    }

    public MutableLiveData<Integer> showFragmentLiveData() {
        return showingFragment;
    }

}
