package com.paradise.newspaper.presentation.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.paradise.newspaper.R;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class FragmentDetailPhoto extends Fragment {

    private static final String ARG_IMAGE_URL = "image_url";
    private String url;

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        this.url = args.getString(ARG_IMAGE_URL);
    }

    public static FragmentDetailPhoto newInstance(String url) {
        FragmentDetailPhoto fragment = new FragmentDetailPhoto();
        Bundle args = new Bundle();
        args.putString(ARG_IMAGE_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_detail, container, false);
        PhotoView detailImageView = inflate.findViewById(R.id.photo_view);
                Glide.with(getActivity())
                .load(url)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .transition(withCrossFade())
                .into(detailImageView);
        return inflate;
    }
}
