package com.paradise.newspaper.presentation.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.paradise.newspaper.R;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;

public class YouTubeVH extends RecyclerView.ViewHolder {
    private YouTubePlayer youTubePlayer;
    private String videoId;
    private YouTubePlayerView playerView;

    public YouTubeVH(@NonNull View itemView) {
        super(itemView);
        playerView = itemView.findViewById(R.id.youtube_player_view);
        playerView.initialize(initializedYouTubePlayer -> {
            initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                @Override
                public void onReady() {
                    youTubePlayer = initializedYouTubePlayer;
                    youTubePlayer.cueVideo(videoId, 0);
                }
            });
        }, true);
    }

    public void cueVideo(String videoId) {
        this.videoId = videoId;
        if (youTubePlayer != null)
            youTubePlayer.cueVideo(videoId, 0);
    }

    public YouTubePlayerView getYouTubePlayerView() {
        return playerView;
    }
}
