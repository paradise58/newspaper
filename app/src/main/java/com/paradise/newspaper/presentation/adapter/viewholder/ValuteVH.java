package com.paradise.newspaper.presentation.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import com.paradise.newspaper.R;
import com.paradise.newspaper.data.entity.news.Valute;

public class ValuteVH extends RecyclerView.ViewHolder {

    private TextView usdValue, eurValue;

    public ValuteVH(@NonNull View itemView) {
        super(itemView);
        usdValue = itemView.findViewById(R.id.USD);
        eurValue = itemView.findViewById(R.id.EUR);
    }

    public void bindView(Valute valute) {
        usdValue.setText(String.format("%s  %s", "\u0024", valute.getUsd()));
        eurValue.setText(String.format("%s  %s", "\u20AC", valute.getEur()));
    }
}
