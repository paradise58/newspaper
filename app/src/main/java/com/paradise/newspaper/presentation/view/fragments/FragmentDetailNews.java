package com.paradise.newspaper.presentation.view.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.paradise.newspaper.R;
import com.paradise.newspaper.common.App;
import com.paradise.newspaper.data.entity.news.Part;
import com.paradise.newspaper.presentation.view.DetailActivity;
import com.paradise.newspaper.presentation.viewmodel.DetailViewModel;
import com.paradise.newspaper.presentation.viewmodel.GlobalViewModelFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.paradise.newspaper.common.Constants.*;

public class FragmentDetailNews extends Fragment {

    @Inject
    GlobalViewModelFactory factory;
    DetailViewModel viewModel;

    private LinearLayout rootViewGroup;
    private LinearLayout.LayoutParams params;
    private final List<String> imageUrlList = new ArrayList<>();

    public static FragmentDetailNews newInstance() {
        return new FragmentDetailNews();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent(getContext()).inject(this);
        viewModel = ViewModelProviders.of(((DetailActivity) getActivity()), factory).get(DetailViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_detail_news, null);
        rootViewGroup = inflate.findViewById(R.id.root_view_group);
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        return inflate;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.getNewsParts().observe(this, this::renderParts);
    }

    private void renderParts(List<Part> data) {
        if (data == null || data.isEmpty()) return;
        rootViewGroup.removeAllViews();
        for (int i = 0; i < data.size(); i++) {
            inflateView(data.get(i));
        }
    }

    private void inflateView(Part part) {
        switch (part.getType()) {
            case PART_PHOTO:
                inflateImageView(part);
                break;
            case PART_TITLE:
                inflateTitle(part);
                break;
            case PART_TEXT:
                inflateText(part);
                break;
            case PART_SUBTITLE:
                inflateSubtitle(part);
                break;
            case PART_LIST_ITEM:
                inflateListItem(part);
                break;
        }
    }

    private void inflateImageView(Part part) {
        ImageView view = new ImageView(getContext());
        view.setAdjustViewBounds(true);
        view.setLayoutParams(params);
        view.setScaleType(ImageView.ScaleType.FIT_CENTER);
        Glide.with(this).load(part.getPayload())
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .transition(withCrossFade())
                .into(view);

        rootViewGroup.addView(view);
        imageUrlList.add(part.getPayload());
        view.setOnClickListener(v -> {
            int position = 0;
            for (int i = 0; i < imageUrlList.size(); i++) {
                if (imageUrlList.get(i).equals(part.getPayload()))
                    position = i;
            }
            viewModel.goToDetailPhoto(position, imageUrlList);
        });
    }

    private void inflateTitle(Part part) {
        TextView view = new TextView(getContext());
        view.setTextSize(20);
        view.setLayoutParams(params);
        Typeface typeface = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(R.font.merriweather_bold);
        }
        view.setTypeface(typeface, Typeface.BOLD);
        view.setPaddingRelative(32, 24, 32, 24);
        view.setTextIsSelectable(true);
        view.setText(part.getPayload());
        view.setGravity(Gravity.CENTER);
        view.setTextColor(Color.parseColor("#000000"));
        rootViewGroup.addView(view);
    }

    private void inflateText(Part part) {
        TextView view = new TextView(getContext());
        view.setTextSize(14);
        view.setPaddingRelative(24, 16, 24, 16);
        Typeface typeface = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(R.font.merriweather_light);
        }
        view.setTypeface(typeface);
        view.setTextIsSelectable(true);
        view.setText(part.getPayload());
        view.setLayoutParams(params);
        view.setGravity(Gravity.START);
        view.setTextColor(Color.parseColor("#000000"));
        rootViewGroup.addView(view);
    }

    private void inflateSubtitle(Part part) {
        TextView view = new TextView(getContext());
        view.setTextSize(16);
        Typeface typeface = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(R.font.merriweather_bold);
        }
        view.setTypeface(typeface, Typeface.BOLD);
        view.setPaddingRelative(24, 16, 24, 16);
        view.setTextIsSelectable(true);
        view.setLayoutParams(params);
        view.setText(part.getPayload());
        view.setGravity(Gravity.CENTER);
        view.setTextColor(Color.parseColor("#000000"));
        rootViewGroup.addView(view);
    }

    private void inflateListItem(Part part) {
        TextView view = new TextView(getContext());
        view.setTextSize(14);
        Typeface typeface = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(R.font.merriweather_bold);
        }
        view.setTypeface(typeface, Typeface.BOLD);
        view.setTextIsSelectable(true);
        view.setText(part.getPayload());
        view.setLayoutParams(params);
        view.setGravity(Gravity.CENTER);
        view.setTextColor(Color.parseColor("#000000"));
        rootViewGroup.addView(view);
    }

}
