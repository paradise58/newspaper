package com.paradise.newspaper.presentation.viewstate;

public class FirstLaunchViewState {

    //Room позволяет получать объект LiveData.

    private boolean launchListEmpty;

    public FirstLaunchViewState(boolean launchListEmpty) {
        this.launchListEmpty = launchListEmpty;
    }

    public boolean isLaunchListEmpty() {
        return launchListEmpty;
    }
}
