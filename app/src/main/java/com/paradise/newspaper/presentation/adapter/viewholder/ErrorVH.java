package com.paradise.newspaper.presentation.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.paradise.newspaper.data.entity.news.News;

public class ErrorVH extends RecyclerView.ViewHolder {


    public ErrorVH(@NonNull View itemView) {
        super(itemView);
    }

    public void bindView(News news) {
        // TODO: 27.10.2018 Здесь надо продумать функционал, который будет сообщать оператору приложения об
        // TODO ошибочно загруженной новости. Это заглушка сработает если оператор некорректно запостил новость

    }
}
