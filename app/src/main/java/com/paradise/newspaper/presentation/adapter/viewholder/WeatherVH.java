package com.paradise.newspaper.presentation.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import com.paradise.newspaper.R;
import com.paradise.newspaper.data.entity.news.Forecast;
import com.paradise.newspaper.presentation.adapter.WeatherDayAdapter;
import com.paradise.newspaper.presentation.interfaces.OnViewPagerClickListener;
import com.paradise.newspaper.presentation.view.custom_view.CollapsedCard;

public class WeatherVH extends RecyclerView.ViewHolder {

    private final FragmentManager fm;
    public CollapsedCard collapsedCard;
    private ViewPager viewPager;
    public TabLayout tabLayout;
    public ImageView ivArrowCloseOpen;

    public WeatherVH(@NonNull View itemView, FragmentManager fm) {
        super(itemView);
        initView(itemView);
        this.fm = fm;
    }

    private void initView(View itemView) {
        collapsedCard = itemView.findViewById(R.id.collaps);
        tabLayout = collapsedCard.getTabLayout();
        viewPager = itemView.findViewById(R.id.view_pager);
        ivArrowCloseOpen = collapsedCard.getIvArrowCloseOpen();
    }

    public void bindView(Forecast forecast, OnViewPagerClickListener listenerView) {
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(new WeatherDayAdapter(fm, forecast, listenerView));
        viewPager.setOffscreenPageLimit(5);
        collapsedCard.bindForecast(forecast);
        collapsedCard.close();
        forecast.setExpanded(false);
        ivArrowCloseOpen.setImageResource(R.drawable.ic_arrow_down);
    }
}
