package com.paradise.newspaper.presentation.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.Handler;
import androidx.work.OneTimeWorkRequest;
import androidx.work.State;
import androidx.work.WorkManager;
import com.paradise.newspaper.common.utils.ToastUtil;
import com.paradise.newspaper.common.utils.WorkUtil;
import com.paradise.newspaper.data.entity.FirstLaunch;
import com.paradise.newspaper.data.network.StubFB;
import com.paradise.newspaper.presentation.viewstate.FirstLaunchViewState;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;
import java.util.List;

public class StartViewModel extends AbsViewModel {

    private final StubFB stubFB;
    private final MutableLiveData<FirstLaunchViewState> firstLaunchLiveData = new MutableLiveData<>();
    private final WorkManager workManager;

    @Inject
    public StartViewModel(StubFB stubFB,
                          WorkManager workManager) {
        this.stubFB = stubFB;
        this.workManager = workManager;

        compositeDisposable.add(getAll().subscribeOn(Schedulers.io())
                .map(List::isEmpty)
                .subscribe(res -> firstLaunchLiveData.postValue(new FirstLaunchViewState(res)),
                        error -> ToastUtil.showLongMessage(error.getLocalizedMessage())));
    }

    public void checkIsLaunched(FirstLaunch firstLaunch) {
        new Thread(() -> stubFB.getFirstLaunchDao().insert(firstLaunch)).start();
    }

    public void insertMockData(){
        stubFB.insertNewsBasic();
        stubFB.insertPartDao();
        stubFB.insertNewsYoutube();
    }

    public LiveData<FirstLaunchViewState> getFirstLaunchLiveData() {
        return firstLaunchLiveData;
    }

    private Observable<List<FirstLaunch>> getAll() {
        return Observable.fromCallable(stubFB.getFirstLaunchDao()::getAll);
    }

    public void startForecastWorker() {
        OneTimeWorkRequest forecastOne = WorkUtil.getOneTimeForecastWorkRequest();
        workManager.enqueue(forecastOne);
        workManager.getStatusById(forecastOne.getId()).observeForever(workStatus -> {
            if (workStatus != null && workStatus.getState().equals(State.SUCCEEDED))
                new Handler().postDelayed(() -> workManager.enqueue(WorkUtil.getPeriodicForecastWorkRequest()), 15000);
        });
    }

}

