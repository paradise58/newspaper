package com.paradise.newspaper.presentation.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import com.paradise.newspaper.R;
import com.paradise.newspaper.common.App;
import com.paradise.newspaper.common.Constants;
import com.paradise.newspaper.common.utils.ToastUtil;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import com.paradise.newspaper.presentation.viewmodel.GlobalViewModelFactory;
import com.paradise.newspaper.presentation.viewmodel.MainViewModel;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    GlobalViewModelFactory factory;
    MainViewModel viewModel;

    private BottomNavigationView bottomNavigationView;
    private NavController navController;
    private int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent(this).inject(this);
        viewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
        restoreBundle(savedInstanceState);
        setContentView(R.layout.activity_main);
        initBottomNavigation();

        viewModel.getBottomSheetState().observe(this, this::renderBottomSheetStateVisibility);
        viewModel.getShowDetail().observe(this, this::renderShowDetail);
    }

    private void renderShowDetail(Long id) {
        if (id == null) return;

        DetailActivity.goToDetailActivity(this, id);
    }

    private void renderBottomSheetStateVisibility(Boolean aBoolean) {
        if (aBoolean == null) return;

        if (aBoolean)
            bottomNavigationView.setVisibility(View.VISIBLE);
        else
            bottomNavigationView.setVisibility(View.GONE);
    }

    private void initBottomNavigation() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.fragment_news_list);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                menuItem -> NavigationUI.onNavDestinationSelected(menuItem, navController));
    }

    public static void goToMainActivity(AppCompatActivity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.CURRENT_POSITION, currentPosition);
        super.onSaveInstanceState(outState);
    }

    private void restoreBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getInt(Constants.CURRENT_POSITION);
        }
    }

    @Override
    public void onBackPressed() {
        //Если нажали назад, то указываем setSelectedItemId
        navController.addOnNavigatedListener((controller, destination) -> {
            switch (String.valueOf(destination.getLabel())) {
                case "fragment1":
                    currentPosition = R.id.fragment1;
                    break;
                case "fragment2":
                    currentPosition = R.id.fragment2;
                    break;
                case "fragment_news_list":
                    currentPosition = R.id.fragment_news_list;
                    break;
                case "fragment4":
                    currentPosition = R.id.fragment4;
                    break;
                case "fragment5":
                    currentPosition = R.id.fragment5;
                    break;
            }
            bottomNavigationView.setSelectedItemId(currentPosition);
        });

        //Если больше нет фрагментов для перехода назад
        if (!navController.popBackStack()) {
            new AlertDialog.Builder(this).setTitle("Точно хочешь уйти?")
                    .setPositiveButton("Да!", (dialog, i) -> super.onBackPressed())
                    .setNeutralButton("Нет!",
                            (dialog, i) -> ToastUtil.showLongMessage("Спасибо, что остались!"))
                    .create()
                    .show();
        }
    }
}
