package com.paradise.newspaper.presentation.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.paradise.newspaper.presentation.view.fragments.FragmentDetailPhoto;
import com.paradise.newspaper.presentation.viewmodel.PhotoDetailState;

public class PhotoDetailAdapter extends FragmentPagerAdapter {

    private PhotoDetailState state;

    public PhotoDetailAdapter(FragmentManager fm, PhotoDetailState state) {
        super(fm);
        this.state = state;
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentDetailPhoto.newInstance(state.getImageUrlList().get(position));
    }

    @Override
    public int getCount() {
        return state.getImageUrlList().size();
    }

}