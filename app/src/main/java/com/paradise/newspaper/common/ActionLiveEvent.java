package com.paradise.newspaper.common;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

public class ActionLiveEvent<T> extends MutableLiveData<T> {

    private boolean isValuePushedOnce = true;

    @MainThread
    @Override
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<T> observer) {
        super.observe(owner, data -> {
            if (isValuePushedOnce)
                return;
            observer.onChanged(data);
            isValuePushedOnce = true;
        });
    }

    @MainThread
    public void sendAction(T data) {
        setValue(data);
    }

    @Override
    public void setValue(T data) {
        isValuePushedOnce = false;
        super.setValue(data);
    }
}

