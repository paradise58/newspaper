package com.paradise.newspaper.common;

import io.reactivex.Scheduler;

public interface SchedulerProvider {
    Scheduler mainThread();

    Scheduler io();

    Scheduler computation();

    Scheduler immediate();
}
