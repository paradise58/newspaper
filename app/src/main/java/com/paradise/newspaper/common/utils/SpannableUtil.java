package com.paradise.newspaper.common.utils;

import com.paradise.newspaper.data.entity.weather.Weather;

public class SpannableUtil {

    public static String parseMaxMinTemp(Weather day) {
//        return String.format("%s%s / %s%s", day.getMaxTemp(), "\u00b0", day.getMinTemp(), "\u00b0");
        return String.format("%s%s", day.getMaxTemp(), "\u00b0");
    }
}
