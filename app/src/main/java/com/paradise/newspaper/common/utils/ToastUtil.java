package com.paradise.newspaper.common.utils;

import android.widget.Toast;
import com.paradise.newspaper.common.App;

public class ToastUtil {

    private ToastUtil() {
    }

    public static void showShortMessage(String message) {
        if (!message.isEmpty()) getToast(message, Toast.LENGTH_SHORT).show();
    }

    public static void showLongMessage(String message) {
        if (!message.isEmpty()) getToast(message, Toast.LENGTH_LONG).show();
    }

    private static Toast getToast(String message, int length) {
        return Toast.makeText(App.get(), message, length);
    }
}
