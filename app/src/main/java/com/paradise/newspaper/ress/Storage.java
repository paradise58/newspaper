package com.paradise.newspaper.ress;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class Storage {

    private static SharedPreferences sharedPreferences;
    private static Storage instrance;
    private static List<StorageListener> storageListeners = new ArrayList<>();

    public static void registerStorageListener(StorageListener listener) {
        storageListeners.add(listener);
    }

    public static void unregisterStorageListener(StorageListener listener) {
        storageListeners.remove(listener);
    }

    public static Storage getInstance(Context context) {
        if (instrance == null)
            instrance = new Storage(context);
        return instrance;
    }

    private Storage(Context context) {
        sharedPreferences = context.getSharedPreferences("pref", Context.MODE_PRIVATE);
    }

    private static RequestListener requestListener = new RequestListener() {
        @Override
        public void onDataLoaded(String text) {
            //Saving changed data
            sharedPreferences.edit().putString("pref", text).apply();

            //Notifying data changed
            for (StorageListener storageListener : storageListeners) {
                storageListener.onDataChanged();
            }
        }
    };

    public static RequestListener getRequestListener() {
        return requestListener;
    }
}
