package com.paradise.newspaper.ress;

public interface StorageListener {
    void onDataChanged();
}
