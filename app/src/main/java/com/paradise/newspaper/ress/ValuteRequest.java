package com.paradise.newspaper.ress;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ValuteRequest {

    private static List<RequestListener> requestListeners = new ArrayList<>();

    public static void makeRequest() {
        new FetchValuteTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static void registerRequestListener(RequestListener listener) {
        requestListeners.add(listener);
    }

    public static void unRegisterRequestListener(RequestListener listener) {
        requestListeners.remove(listener);
    }


    public static class FetchValuteTask extends AsyncTask<Void, Void, String> {

        private static final String CBR = "https://www.cbr-xml-daily.ru/daily_json.js";
//        public static final String CBR = "http://www.cbr.ru/scripts/XML_daily.asp";

        @Override
        protected String doInBackground(Void... voids) {
            try {
                return new String(fetchDataFromCBR());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @SuppressLint("DefaultLocale")
        @Override
        protected void onPostExecute(String fetchedData) {
            if (fetchedData.equals("")) return;

            StringBuilder builder = new StringBuilder();
            try {
                JSONObject jsonBody = new JSONObject(fetchedData);
                JSONObject valute = jsonBody.getJSONObject("Valute");
                Iterator x = valute.keys();
                JSONArray valutes = new JSONArray();
                while (x.hasNext()) {
                    String key = (String) x.next();
                    valutes.put(valute.get(key));
                }

                for (int i = 0; i < valutes.length(); i++) {
                    JSONObject val = valutes.getJSONObject(i);
                    if (val.getString("CharCode").equals("USD")) {
                        builder.append(String.format("%.4g%n", val.getDouble("Value")));
                        builder.append(":");
                    }
                    if (val.getString("CharCode").equals("EUR")) {
                        builder.append(String.format("%.4g%n", val.getDouble("Value")));
                        builder.append(":");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            long fetchedDate = new Date().getTime() / 1000L;
            builder.append(String.valueOf(fetchedDate));

            ValuteRequest.onHandleListener(builder.toString());
        }

        private byte[] fetchDataFromCBR() throws IOException {
            URL url = new URL(CBR);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                InputStream is = connection.getInputStream();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                    throw new IOException(connection.getResponseMessage());

                int bytesRead;
                byte[] buffer = new byte[1024];

                while ((bytesRead = is.read(buffer)) > 0) {
                    out.write(buffer, 0, bytesRead);
                }
                out.close();
                return out.toByteArray();
            } finally {
                connection.disconnect();
            }
        }
    }

    private static void onHandleListener(String result) {
        for (RequestListener listener : requestListeners) {
            listener.onDataLoaded(result);
        }
    }
}
