package com.paradise.newspaper.ress;

public interface RequestListener {
    void onDataLoaded(String text);
}
