package com.paradise.newspaper.worker;

import android.support.annotation.NonNull;
import androidx.work.Worker;
import com.paradise.newspaper.common.App;
import com.paradise.newspaper.common.SchedulerProvider;
import com.paradise.newspaper.common.utils.ToastUtil;
import com.paradise.newspaper.data.entity.weather.Weather;
import com.paradise.newspaper.data.entity.weather.fiveDays.FiveDaysWeatherResult;
import com.paradise.newspaper.data.entity.weather.fiveDays.List;
import com.paradise.newspaper.data.entity.weather.fiveDays.Main;
import com.paradise.newspaper.data.network.IWeatherClient;
import com.paradise.newspaper.data.network.StubFB;
import io.reactivex.disposables.Disposable;

import javax.inject.Inject;
import java.util.ArrayList;

import static com.paradise.newspaper.common.utils.TimeUtil.todayTimeStamp;
import static com.paradise.newspaper.common.utils.TimeUtil.weatherTimeStamp;

public class ForecastWorker extends Worker {

    @Inject
    StubFB stubFB;
    @Inject
    IWeatherClient client;
    @Inject
    SchedulerProvider schedulerProvider;

    @NonNull
    @Override
    public Result doWork() {
        App.getAppComponent(getApplicationContext()).inject(this);
        java.util.List<Weather> weatherList = stubFB.getNewsWeatherDao().getList();
        if (weatherList.isEmpty()) {
            fetchForecastFromApi(weatherList);
        }

        if (!weatherList.isEmpty() && todayTimeStamp() != weatherTimeStamp(weatherList.get(0).getDt())) {
            fetchForecastFromApi(weatherList);
        }

        return Result.SUCCESS;
    }

    private void fetchForecastFromApi(java.util.List<Weather> weatherList) {
        Disposable disposable = client.getForecastByLatLng()
                .subscribeOn(schedulerProvider.io())
                .doOnSubscribe(fiveDaysWeatherResult -> stubFB.getNewsWeatherDao().deleteAll(weatherList))
                .subscribe(weatherResult -> {
                            if (weatherResult.getCod().equals("200")) {
                                saveForecastToDB(weatherResult);
                            }
                        },
                        error -> ToastUtil.showLongMessage(error.getLocalizedMessage()));
    }

    private void saveForecastToDB(FiveDaysWeatherResult weatherResult) {
        java.util.List<List> lists = weatherResult.getList();
        ArrayList<Weather> weathers = new ArrayList<>();
        for (int i = 0; i < weatherResult.getCnt(); i++) {
            List list = lists.get(i);
            Main main = list.getMain();
            Weather weather = new Weather();
            weather.setDescription(list.getWeather().get(0).getDescription());
            weather.setIcon(list.getWeather().get(0).getIcon());
            weather.setTemp((int) Math.round(main.getTemp()));
            weather.setMaxTemp((int) Math.round(main.getTempMax()));
            weather.setMinTemp((int) Math.round(main.getTempMin()));
            weather.setHumidity(main.getHumidity());
            weather.setPressure((int) Math.round(main.getPressure() / 1.33));
            weather.setWindSpeed((int) Math.round(list.getWind().getSpeed()));
            weather.setDt(list.getDt());
            weather.setDtTxt(list.getDtTxt());
            weathers.add(weather);
        }
        new Thread(() -> stubFB.getNewsWeatherDao().insert(weathers)).start();
    }

}
