package com.paradise.newspaper.di.module;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.paradise.newspaper.di.scope.AppScope;
import com.paradise.newspaper.data.network.IWeatherClient;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

@Module
public class NetworkModule {

    @Provides
    @AppScope
    IWeatherClient providesIWeatherClient() {
        return new Retrofit.Builder().baseUrl("https://api.openweathermap.org/data/2.5/")
                .client(new OkHttpClient.Builder().connectTimeout(3, TimeUnit.SECONDS)
                        .readTimeout(3, TimeUnit.SECONDS)
                        .writeTimeout(3, TimeUnit.SECONDS)
                        .addNetworkInterceptor(new StethoInterceptor())
                        .build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(IWeatherClient.class);
    }
}