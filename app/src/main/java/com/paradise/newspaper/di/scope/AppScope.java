package com.paradise.newspaper.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

//Объекты помеченные этим скоупом живут на протяжении жизни приложения

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppScope {
}
