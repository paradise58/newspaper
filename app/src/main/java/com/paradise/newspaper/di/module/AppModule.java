package com.paradise.newspaper.di.module;

import android.content.Context;
import androidx.work.WorkManager;
import com.paradise.newspaper.common.App;
import com.paradise.newspaper.common.MainSchedulerProvider;
import com.paradise.newspaper.common.SchedulerProvider;
import com.paradise.newspaper.di.scope.AppScope;
import dagger.Module;
import dagger.Provides;

@Module(includes = {
        ViewModelModule.class, RoomModule.class, NetworkModule.class, InteractorModule.class
})

public class AppModule {

    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    Context provideContext() {
        return app;
    }

    @Provides
    App provideApp() {
        return app;
    }

    @Provides
    @AppScope
    SchedulerProvider provideSchedulerProvider() {
        return new MainSchedulerProvider();
    }

    @Provides
    @AppScope
    WorkManager provideAndroidWorkManager() {
        return WorkManager.getInstance();
    }
}
