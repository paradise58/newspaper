package com.paradise.newspaper.di.component;

import com.paradise.newspaper.di.module.AppModule;
import com.paradise.newspaper.di.scope.AppScope;
import com.paradise.newspaper.presentation.view.DetailActivity;
import com.paradise.newspaper.presentation.view.MainActivity;
import com.paradise.newspaper.presentation.view.NewsListFragment;
import com.paradise.newspaper.presentation.view.StartActivity;
import com.paradise.newspaper.presentation.view.fragments.FragmentDetailNews;
import com.paradise.newspaper.presentation.view.fragments.FragmentDetailPhotoContainer;
import com.paradise.newspaper.worker.ForecastWorker;
import dagger.Component;

@AppScope
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(StartActivity activity);

    void inject(MainActivity activity);

    void inject(DetailActivity activity);

    void inject(NewsListFragment fragment);

    void inject(ForecastWorker worker);

    void inject(FragmentDetailPhotoContainer fragmentDetailPhotoContainer);

    void inject(FragmentDetailNews fragmentDetailNews);
}
